package com.besaba.okomaru_server.junT58.ColorGlassResistance;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;

public class Main extends JavaPlugin implements Listener{

	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
		if(board.getPlayerTeam(p) != null){
			String tn = board.getPlayerTeam(p).getName();
			int dmg = getConfig().getInt("Colors." + tn);
			if(p.getLocation().clone().add(0, -1, 0).getBlock().getType().equals(Material.STAINED_GLASS)){
				if(p.getLocation().clone().add(0, -1, 0).getBlock().getData() == dmg){
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 3*20, 100));
				}
			}
		}
	}

	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args){
		if(args.length <= 1){
			return false;
		}
		String team = args[0];
		String d = args[1];
		int dmg = 0;
		try {
			dmg = Integer.parseInt(d);
		} catch(NumberFormatException e){
			return false;
		}
		getConfig().set("Colors." + team, dmg);
		saveConfig();
		sender.sendMessage("OK");
		return true;
	}

}
